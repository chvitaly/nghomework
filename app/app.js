angular.module('homework', []);

angular.module('homework').constant("LANGS", {
                        'DE':'GERMAN', 
                        'EN':'ENGLISH',
                        'IT':'ITALIAN',
                        'FR':'FRANCH' 
                    });
                    
angular.module('homework').constant("ROLES", {
                        'UP':'НОВИЧЁК', 
                        'AD':'РЯДОВОЙ',
                        'VI':'БОСС',
                        'SU':'СЛУГА',
                        'AU':'РАБ'
});

angular.module('homework').constant("ROLES_NONE", "НИКТО");

angular.module('homework').filter('langF', function(LANGS) {
        return function(input) {
            return LANGS[input];
        }
        })
      
        .filter('mroleF', function(ROLES, ROLES_NONE) {
        return function(input) {
            var result = ROLES_NONE;
                    
            if (ROLES[input])
            {
                result = ROLES[input];
            } else return input;
            
            return result;
        };
        })
        
        .filter('dateF', function() {
        return function(input) {
            return moment(input).startOf('hours').fromNow();
        };
        })
;

// Tiny controller to expose current page to the main menu
angular.module('homework').controller('Main', function($scope, $filter, Loader){
    
    var users_on_page = 10;
    
    $scope.TakeError = false;
    
    $scope.page = {
        start: 0,
        end: (users_on_page*1)
    };
    
    $scope.users = [];
    
    $scope.sort = {
        field: 'updated',
        order_asc: true
    };
    
    $scope.showLoadMore = true;
    $scope.selectedUsers = 0;
    
    $scope.filters = {};
    
    $scope.changeGettingError = function()
    {
        //TakeError = !TakeError;
        $scope.TakeError = !$scope.TakeError;
    };
    
    $scope.toFilter = function() {
        loadUsers();
    };
    
    $scope.loadMoreUsers = function() {
        $scope.page.end += users_on_page;
        loadUsers();
    };
    
    $scope.orderByField = function(sort_field)
    {
        if ($scope.sort.field === sort_field) {
            $scope.sort.order_asc = !$scope.sort.order_asc;
        } else $scope.sort.order_asc = true;
        
        $scope.sort.field = sort_field;
        
        $scope.page.start = 0;
        $scope.page.end = users_on_page*1;
        
        $scope.filters = {};
        
        
        loadUsers();
    };
    
    $scope.deleteUser = function(user)
    {        
        var params = {
            id: user.id
        };
        
        Loader
            .delete('/api/users/'+ user.id)
            .then(function(response) {
                $scope.users.splice( $scope.users.indexOf(user), 1);
            });
    };
    
    $scope.selectUser = function(user)
    {
        if (!user.selected)
        {
            user.selected = true;
        } else user.selected = !user.selected;
    };
    
    $scope.$watch('users', function (newVal, oldVal) {
        $scope.selectedUsers = $filter('filter')($scope.users, {selected: true}).length;
    }, true);
    
    $scope.deleteSelectedUsers = function()
    {
        for (var usr in $scope.users)
        {
            if ($scope.users[usr].selected) {
                $scope.deleteUser($scope.users[usr]);
            }
        }
    };
        
    function loadUsers() {
        var params = {
                    start: $scope.page.start, 
                    end:   $scope.page.end,
                    sort:  $scope.sort.field,
                    order: $scope.sort.order_asc?'asc':'desc'
                };
                
        if ($scope.TakeError)
        {
            params["error"] = true;
        }
                
        //angular.extend(params, $scope.filters);                
        for(var key in $scope.filters)
        {
            if ($scope.filters[key])
                params[key] = $scope.filters[key];
        }
        
        Loader
            .get('/api/users', { params })
            .then(function(response) {
                if (response.data)
                {
                    $scope.users = response.data.data;
                }

                // hide load more
                if (response.data.length < (users_on_page))
                {
                    $scope.showLoadMore = false;
                } 
            });
    };

    loadUsers();
});

// Simple directive to show / hide the spinner while loader is busy
angular.module('homework').directive('xtLoader', function(Loader){
    return {
        restrict: 'E',
        replace: true,
        scope: {},
        templateUrl: function($element, $attrs){
                return $attrs.template;
        },
        link: function($scope, $element, $attrs){
                $scope.loader = Loader;
        }
    };
});

// Simple directive to show / hide errors
angular.module('homework').directive('xtError', function(Loader){
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function($element, $attrs){
                return $attrs.template;
        },
        link: function($scope, $element, $attrs){
                $scope.loader = Loader;
        }
    };
});

// Service mocking asynch http GET and POST requests
angular.module('homework').service('Loader', function($q, $http, $timeout){
    var _this = this;

    // Public property to expose service busy state
    _this.busy = false;
    _this.errors = [];

    _this.get = function(link, params){
        _this.busy = true;
        return $http
            .get(link, params)
            .then( // or finally
              function(response) {
                  // on success
                _this.busy = false;
                return response;
              }, 
              function(response) {
                  // an error happen
                _this.busy = false;
                
                _this.errors.unshift("Error: " + response.data.error + '   ' + new Date().getTime());
                $timeout(function() {
                    _this.errors.pop();
                }, 10000);
                
                return response;
            });
    };

    _this.delete = function(link){ 
        _this.busy = true;
        return $http
            .delete(link)
            .then( // or finally
              function(response) {
                _this.busy = false;
                return response;
              }, 
              function(response) {
                _this.busy = false;
                return response;
            });
    };
});

// Multipurpose filters component
angular.module('homework').directive('xtFilters', ['LANGS', 'ROLES', function(LANGS, ROLES){
    return {
        restrict: 'E',
        replace: true,
        scope: {
            filters: '=filters',
            change: '&change'
        },
        templateUrl: function($element, $attrs){
                return $attrs.template;
        },
        link: function($scope, $element, $attrs){
            // Clearing the filters
            
            $scope.languages = LANGS;
            $scope.roles = ROLES;
            $scope.clear = function(){
                for(var key in $scope.filters)
                {
                    $scope.filters[key] = '';
                }
                $scope.change();
            };
        }
    };
}]);